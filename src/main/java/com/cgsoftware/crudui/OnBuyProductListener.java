/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.cgsoftware.crudui;

import com.cgsoftware.crudui.model.Product;

/**
 *
 * @author natthawadee
 */
public interface OnBuyProductListener {
    public void buy(Product product, int amount);
}
