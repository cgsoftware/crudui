/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cgsoftware.crudui.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class Material {
    int id;
    String name;
    int min;
    int current;
    String unit;
    int buy = 0;
    int matPrice;

    public int getBuy() {
        return buy;
    }

    public void setBuy(int buy) {
        this.buy = buy;
    }

    public Material(int id, String name, int min, int current, String unit, int matPrice) {
        this.id = id;
        this.name = name;
        this.min = min;
        this.current = current;
        this.unit = unit;
        this.matPrice = matPrice;
    }

    public Material(String name, int min, int current, String unit) {
        this.id = -1;
        this.name = name;
        this.min = min;
        this.current = current;
        this.unit = unit;
    }
    
    public Material() {
        this.id = -1;
        this.name = "";
        this.min = 0;
        this.current = 0;
        this.unit = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getCurrent() {
        return current;
    }

    public void setCurrent(int current) {
        this.current = current;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "" + name + " x" + buy + " " + unit;
    }

    public int getMatPrice() {
        return matPrice;
    }

    public void setMatPrice(int matPrice) {
        this.matPrice = matPrice;
    }
    
    
    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("mat_id"));
            material.setName(rs.getString("mat_name"));
            material.setMin(rs.getInt("mat_min"));
            material.setCurrent(rs.getInt("mat_current"));
            material.setUnit(rs.getString("mat_unit"));
            material.setMatPrice(rs.getInt("mat_price"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }
}
