/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgsoftware.crudui.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Black Dragon
 */
public class TimeAttendance {

    public static TimeAttendance fromRS(ResultSet rs) {
       TimeAttendance ta = new TimeAttendance();
        try {
            ta.setId(rs.getInt("ta_id"));
            ta.setCheckInTime(rs.getString("ta_check_in_time"));
            ta.setCheckOutTime(rs.getString("ta_check_in_time"));
            ta.setOtHour(rs.getInt("ta_applied_OT_hour"));
            ta.setStatus(rs.getString("ta_status"));
            ta.setEmpId(rs.getInt("employee_id"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return ta;
    }
    private int id;
    private String checkInTime;
    private String checkOutTime;
    private int otHour;
    private String status;
    private int empId;

    public TimeAttendance() {
    }
    
    public TimeAttendance(int empId) throws ParseException {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.checkInTime = dateFormat.format(date);
        this.empId = empId;
        id = -1;
        otHour = 0;
        SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
        Date tmpdate = parser.parse("08:00");
        if(date.before(tmpdate)) {
            status = "Normal";
        }else{
            status = "Late";
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(String checkInTime) {
        this.checkInTime = checkInTime;
    }

    public String getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(String checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public int getOtHour() {
        return otHour;
    }

    public void setOtHour(int otHour) {
        this.otHour = otHour;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public void initCheckOutTime() {
        Date date = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        checkOutTime = dateFormat.format(date);
    }
}
