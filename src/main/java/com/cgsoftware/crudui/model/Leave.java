/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cgsoftware.crudui.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class Leave {

    int leaveId;
    String detail;
    String startDate;
    String finishDate;
    String startTime;
    String endTime;
    String amountDate;
    String amountTime;
    int empId;

    public Leave(int leaveId, String detail, String startDate, String finishDate, String startTime, String endTime, String amountDate, String amountTime, int empId) {
        this.leaveId = leaveId;
        this.detail = detail;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.amountDate = amountDate;
        this.amountTime = amountTime;
        this.empId = empId;
    }

    public Leave(String detail, String startDate, String finishDate, String startTime, String endTime, String amountDate, String amountTime, int empId) {
        this.leaveId = -1;
        this.detail = detail;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.amountDate = amountDate;
        this.amountTime = amountTime;
        this.empId = empId;
    }

    public Leave() {
        this.leaveId = -1;
        this.detail = "";
        this.startDate = "";
        this.finishDate = "";
        this.startTime = "";
        this.endTime = "";
        this.amountDate = "";
        this.amountTime = "";
        this.empId = -1;
    }

    public int getLeaveId() {
        return leaveId;
    }

    public void setLeaveId(int leaveId) {
        this.leaveId = leaveId;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getAmountDate() {
        return amountDate;
    }

    public void setAmountDate(String amountDate) {
        this.amountDate = amountDate;
    }

    public String getAmountTime() {
        return amountTime;
    }

    public void setAmountTime(String amountTime) {
        this.amountTime = amountTime;
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    @Override
    public String toString() {
        return "Leave{" + "leaveId=" + leaveId + ", detail=" + detail + ", startDate=" + startDate + ", finishDate=" + finishDate + ", startTime=" + startTime + ", endTime=" + endTime + ", amountDate=" + amountDate + ", amountTime=" + amountTime + ", empId=" + empId + '}';
    }
    
    public static Leave fromRS(ResultSet rs) {
        Leave leave = new Leave();
        try {
            leave.setLeaveId(rs.getInt("leave_id"));
            leave.setDetail(rs.getString("leave_Detail"));
            leave.setStartDate(rs.getString("leave_StartDate"));
            leave.setFinishDate(rs.getString("leave_FinishDate"));
            leave.setStartTime(rs.getString("leave_Starttime"));
            leave.setEndTime(rs.getString("leave_Endtime"));
            leave.setAmountDate(rs.getString("leave_Amount_Date"));
            leave.setAmountTime(rs.getString("leave_Amount_Time"));
            leave.setEmpId(rs.getInt("employee_id"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return leave;
    }
}
