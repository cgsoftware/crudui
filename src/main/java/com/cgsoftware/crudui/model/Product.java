/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgsoftware.crudui.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author EAK
 */
public class Product {

    private int id;
    private String name;
    private double price;
    private String size;
    private int sweetLevel;
    private String type;
    private int categoryId;
    private int sale;

    public Product(int id, String name, double price, String size, int sweetLevel, String type, int categoryId) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.size = size;
        this.sweetLevel = sweetLevel;
        this.type = type;
        this.categoryId = categoryId;
    }

    public Product(String name, int sale) {
        this.name = name;
        this.sale = sale;
    }

    public Product(int id, String name, double price) {
        this.id = id;
        this.name = name;
        this.price = price;
    }
    
    public Product() {
        this.id = -1;
        this.name = "";
        this.price = 0;
        this.size = "-";
        this.sweetLevel = 0;
        this.type = "-";
        this.categoryId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public int getSweetLevel() {
        return sweetLevel;
    }

    public void setSweetLevel(int sweetLevel) {
        this.sweetLevel = sweetLevel;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getSale() {
        return sale;
    }

    public void setSale(int sale) {
        this.sale = sale;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price+ '}';
    }
    
    public String toString(int amount) {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price*amount+ '}';
    }
    
    public static ArrayList<Product> getMockProductList(){
        ArrayList<Product> list = new ArrayList<Product>();
        for(int i=0; i<19; i++){
            list.add(new Product(i+1, "Coffee " + i+1, i*5));
        }
        return list;
    }
    
    public static Product fromRS(ResultSet rs) {
        Product prod = new Product();
        try {
            prod.setId(rs.getInt("product_id"));
            prod.setName(rs.getString("product_name"));
            prod.setPrice(rs.getDouble("product_price"));
            prod.setSize(rs.getString("product_size"));
            prod.setSweetLevel(rs.getInt("product_sweet_level"));
            prod.setType(rs.getString("product_type"));
            prod.setCategoryId(rs.getInt("category_id"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return prod;
    }

}
