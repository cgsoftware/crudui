/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgsoftware.crudui.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Black Dragon
 */
public class Order {

    public static Order fromRS(ResultSet rs) {
        Order order = new Order();
        try {
            order.setId(rs.getInt("order_id"));
            order.setProdName(rs.getString("product_name"));
            order.setPrice(rs.getInt("total_price"));
            order.setDate(rs.getString("date"));
            order.setDiscount(rs.getInt("total_discount"));
            order.setEmpId(rs.getString("employee_id"));
            order.setCusId((rs.getInt("customer_id")));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return order;
    }
    
    
    private int id;
    private String prodName;
    private int price;
    private String date;
    private int discount;
    private String empId;
    private int cusId;

    public Order() {
        id = -1;
        prodName = "";
        price = 0;
        date = "";
        discount = 0;
        empId = "-";
        cusId = 0;
    }

    public Order(int id, String name, int price, String date, int discount, String empId, int cusId) {
        this.id = id;
        this.prodName = name;
        this.price = price;
        this.date = date;
        this.discount = discount;
        this.empId = empId;
        this.cusId = cusId;
    }

    public Order(String name, int price, String date, String empId) {
        id = -1;
        this.prodName = name;
        this.price = price;
        this.date = date;
        discount = 0;
        this.empId = empId;
        cusId = 0;
    }

    public Order(String name, int price, String date, String empId, int cusId) {
        id = -1;
        discount = 10;
        this.prodName = name;
        this.price = price - discount;
        this.date = date;
        this.empId = empId;
        this.cusId = cusId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProdName() {
        return prodName;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getEmpId() {
        return empId;
    }

    public void setEmpId(String empId) {
        this.empId = empId;
    }

    public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    @Override
    public String toString() {
        return "Order{" + "id=" + id + ", prodName=" + prodName + ", price=" + price + ", date=" + date + ", discount=" + discount + ", empId=" + empId + ", cusId=" + cusId + '}';
    }
}
