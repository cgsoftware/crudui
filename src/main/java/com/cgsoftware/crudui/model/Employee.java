/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cgsoftware.crudui.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author informatics
 */
public class Employee {
    int id;
    String firstname;
    String lastname;
    String doa;
    String dob;
    String gender;
    String address;
    String phone;
    int salary;
    int mSalary;

    public Employee(int id, String firstname, String lastname, String doa, String dob, String gender, String address, String phone, int salary) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.doa = doa;
        this.dob = dob;
        this.gender = gender;
        this.address = address;
        this.phone = phone;
        this.salary = salary;
    }
    
    public Employee(String firstname, String lastname, String doa, String dob, String gender, String address, String phone, int salary) {
        this.id = -1;
        this.firstname = firstname;
        this.lastname = lastname;
        this.doa = doa;
        this.dob = dob;
        this.gender = gender;
        this.address = address;
        this.phone = phone;
        this.salary = salary;
    }
    
    public Employee() {
        this.id = -1;
        this.firstname = "";
        this.lastname = "";
        this.doa = "";
        this.dob = "";
        this.gender = "Male";
        this.address = "";
        this.phone = "";
        this.salary = 0;
    }

    public Employee(int id, String firstname, String lastname, int mSalary) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.mSalary = mSalary;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getDoa() {
        return doa;
    }

    public void setDoa(String doa) {
        this.doa = doa;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public int getmSalary() {
        return mSalary;
    }

    public void setmSalary(int mSalary) {
        this.mSalary = mSalary;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", firstname=" + firstname + ", lastname=" + lastname + ", doa=" + doa + ", dob=" + dob + ", gender=" + gender + ", address=" + address + ", phone=" + phone + ", salary=" + salary + '}';
    }
    
    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("employee_id"));
            employee.setFirstname(rs.getString("firstname"));
            employee.setLastname(rs.getString("lastname"));
            employee.setDoa(rs.getString("date_of_attendance"));
            employee.setDob(rs.getString("date_of_birth"));
            employee.setGender(rs.getString("gender"));
            employee.setAddress(rs.getString("address"));
            employee.setPhone(rs.getString("phone"));
            employee.setSalary(rs.getInt("daily_salary"));
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }
    
}
