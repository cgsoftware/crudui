/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.cgsoftware.crudui;

import com.cgsoftware.crudui.dao.UserDao;
import com.cgsoftware.crudui.model.User;
import com.cgsoftware.crudui.service.UserService;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

/**
 *
 * @author informatics
 */
public class Login extends javax.swing.JPanel {
    
    JScrollPane scrPane;
            
    /**
     * Creates new form Login
     */
    public Login(JScrollPane scrPane) {
        initComponents();
        this.scrPane = scrPane;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        usernameLabel = new javax.swing.JLabel();
        passwordLabel = new javax.swing.JLabel();
        usernameField = new javax.swing.JTextField();
        passwordField = new javax.swing.JPasswordField();
        btnLogin = new javax.swing.JButton();
        logo = new javax.swing.JLabel();
        coffee = new javax.swing.JLabel();
        leaf1 = new javax.swing.JLabel();
        leaf2 = new javax.swing.JLabel();
        bg = new javax.swing.JLabel();

        setPreferredSize(new java.awt.Dimension(800, 600));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        usernameLabel.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        usernameLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        usernameLabel.setText("Username :");
        add(usernameLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 380, 124, 41));

        passwordLabel.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        passwordLabel.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        passwordLabel.setText("Password :");
        add(passwordLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 440, 124, -1));

        usernameField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                usernameFieldActionPerformed(evt);
            }
        });
        add(usernameField, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 390, 200, 29));

        passwordField.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                passwordFieldActionPerformed(evt);
            }
        });
        add(passwordField, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 440, 200, 29));

        btnLogin.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnLogin.setText("Login");
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
        add(btnLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(910, 510, 100, -1));

        logo.setIcon(new javax.swing.ImageIcon(".\\image\\groupname.png")); // NOI18N
        add(logo, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 180, 980, 220));

        coffee.setIcon(new javax.swing.ImageIcon(".\\image\\rawmaterial.jpeg")); // NOI18N
        add(coffee, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 450, 790, 650));

        leaf1.setIcon(new javax.swing.ImageIcon(".\\image\\coffee.jpg")); // NOI18N
        add(leaf1, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, -420, 1820, 2410));

        leaf2.setIcon(new javax.swing.ImageIcon(".\\image\\coffee.jpg")); // NOI18N
        add(leaf2, new org.netbeans.lib.awtextra.AbsoluteConstraints(-1210, -910, 1980, 2410));

        bg.setIcon(new javax.swing.ImageIcon(".\\image\\backwhite.png")); // NOI18N
        bg.setText("jLabel1");
        add(bg, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 2050, 1200));
    }// </editor-fold>//GEN-END:initComponents

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        String username = usernameField.getText();
        String password = new String(passwordField.getPassword());
        UserService userService = new UserService();
        User user = userService.login(username, password);
        if(user != null) {
            if(user.getRole().equals("manager")) {
                scrPane.setViewportView(new MainMenuOfManager(user.getId(),scrPane));
            }else if(user.getRole().equals("employee")) {
                scrPane.setViewportView(new MainMenuOfStaff(user.getId(),scrPane));
            }
        }else{
            System.out.println("Login Failed");
        }
    }//GEN-LAST:event_btnLoginActionPerformed

    private void usernameFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_usernameFieldActionPerformed
        btnLoginActionPerformed(evt);
    }//GEN-LAST:event_usernameFieldActionPerformed

    private void passwordFieldActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_passwordFieldActionPerformed
        btnLoginActionPerformed(evt);
    }//GEN-LAST:event_passwordFieldActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel bg;
    private javax.swing.JButton btnLogin;
    private javax.swing.JLabel coffee;
    private javax.swing.JLabel leaf1;
    private javax.swing.JLabel leaf2;
    private javax.swing.JLabel logo;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JLabel passwordLabel;
    private javax.swing.JTextField usernameField;
    private javax.swing.JLabel usernameLabel;
    // End of variables declaration//GEN-END:variables
}