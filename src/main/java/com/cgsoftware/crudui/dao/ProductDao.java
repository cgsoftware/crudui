/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgsoftware.crudui.dao;

import com.cgsoftware.crudui.helper.DatabaseHelper;
import com.cgsoftware.crudui.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Black Dragon
 */
public class ProductDao {
    public List<Product> getAll() {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product prod = Product.fromRS(rs);
                list.add(prod);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public void delete(Product obj) {
        String sql = "DELETE FROM product WHERE product_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void save(Product obj) {
        String sql = "INSERT INTO product (product_name, product_price, product_size, product_sweet_level, product_type, category_id)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getPrice());     
            stmt.setString(3, obj.getSize());
            stmt.setInt(4, obj.getSweetLevel());
            stmt.setString(5, obj.getType());
            stmt.setInt(6, obj.getCategoryId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void update(Product obj) {
        String sql = "UPDATE product"
                + " SET product_name = ?, product_price = ?, product_size = ?, product_sweet_level = ?, product_type = ?, category_id = ?"
                + " WHERE product_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getPrice());     
            stmt.setString(3, obj.getSize());
            stmt.setInt(4, obj.getSweetLevel());
            stmt.setString(5, obj.getType());
            stmt.setInt(6, obj.getCategoryId());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
