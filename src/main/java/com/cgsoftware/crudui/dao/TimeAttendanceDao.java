/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgsoftware.crudui.dao;

import com.cgsoftware.crudui.helper.DatabaseHelper;
import com.cgsoftware.crudui.model.TimeAttendance;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Black Dragon
 */
public class TimeAttendanceDao {
    public TimeAttendance save(TimeAttendance obj) {

        String sql = "INSERT INTO TimeAttendance (ta_check_in_time, ta_check_out_time, ta_applied_OT_hour, ta_status, employee_id)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCheckInTime());
            stmt.setString(2, obj.getCheckOutTime());
            stmt.setInt(3, obj.getOtHour());
            stmt.setString(4, obj.getStatus());
            stmt.setInt(5, obj.getEmpId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public void checkOut(int userId, TimeAttendance obj) {
        String sql = "UPDATE TimeAttendance"
                + " SET ta_check_out_time = ?"
                + " WHERE  ta_check_in_time = ? AND employee_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getCheckOutTime());
            stmt.setString(2, obj.getCheckInTime());
            stmt.setInt(3, userId);
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public TimeAttendance getWhere(int userId) {
        TimeAttendance ta = null;
        String sql = "SELECT * FROM TimeAttendance WHERE employee_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, userId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ta = TimeAttendance.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return ta;
    }
}
