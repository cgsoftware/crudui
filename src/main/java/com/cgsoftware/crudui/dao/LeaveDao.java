/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cgsoftware.crudui.dao;

import com.cgsoftware.crudui.helper.DatabaseHelper;
import com.cgsoftware.crudui.model.Leave;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class LeaveDao {

    public List<Leave> getAll() {
        ArrayList<Leave> list = new ArrayList();
        String sql = "SELECT * FROM Leave";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Leave leave = Leave.fromRS(rs);
                list.add(leave);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public Leave save(Leave obj) {

        String sql = "INSERT INTO Leave (leave_Detail,\n"
                + "                      leave_StartDate,\n"
                + "                      leave_FinishDate,\n"
                + "                      leave_Starttime,\n"
                + "                      leave_Endtime,\n"
                + "                      leave_Amount_Date,\n"
                + "                      leave_Amount_Time,\n"
                + "                      employee_id)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getDetail());
            stmt.setString(2, obj.getStartDate());
            stmt.setString(3, obj.getFinishDate());
            stmt.setString(4, obj.getStartTime());
            stmt.setString(5, obj.getEndTime());
            stmt.setString(6, obj.getAmountDate());
            stmt.setString(7, obj.getAmountTime());
            stmt.setInt(8, obj.getEmpId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setLeaveId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public Leave update(Leave obj) {
        String sql = "UPDATE Leave"
                + " SET leave_Detail = ?,\n"
                + " leave_StartDate = ?,\n"
                + " leave_FinishDate = ?,\n"
                + " leave_Starttime = ?,\n"
                + " leave_Endtime = ?,\n"
                + " leave_Amount_Date = ?,\n"
                + " leave_Amount_Time = ?,\n"
                + " employee_id = ?"
                + " WHERE leave_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getDetail());
            stmt.setString(2, obj.getStartDate());
            stmt.setString(3, obj.getFinishDate());
            stmt.setString(4, obj.getStartTime());
            stmt.setString(5, obj.getEndTime());
            stmt.setString(6, obj.getAmountDate());
            stmt.setString(7, obj.getAmountTime());
            stmt.setInt(8, obj.getEmpId());
            stmt.setInt(9, obj.getLeaveId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(Leave obj) {
        String sql = "DELETE FROM Leave WHERE leave_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getLeaveId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }
}
