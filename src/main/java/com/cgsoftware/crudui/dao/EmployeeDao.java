/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cgsoftware.crudui.dao;

import com.cgsoftware.crudui.helper.DatabaseHelper;
import com.cgsoftware.crudui.model.Employee;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class EmployeeDao {
    public List<Employee> getAll() {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT * FROM Employee";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Employee emp = Employee.fromRS(rs);
                list.add(emp);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public Employee save(Employee obj) {

        String sql = "INSERT INTO Employee (firstname, lastname, date_of_attendance, date_of_birth, gender, address, phone, daily_salary)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getFirstname());
            stmt.setString(2, obj.getLastname());
            stmt.setString(3, obj.getDoa());
            stmt.setString(4, obj.getDob());
            stmt.setString(5, obj.getGender());
            stmt.setString(6, obj.getAddress());
            stmt.setString(7, obj.getPhone());
            stmt.setInt(8, obj.getSalary());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public Employee update(Employee obj) {
        String sql = "UPDATE Employee"
                + " SET firstname = ?, lastname = ?, date_of_attendance = ?,"
                + " date_of_birth = ?, gender = ?, address = ?, phone = ?, daily_salary = ?"
                + " WHERE employee_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getFirstname());
            stmt.setString(2, obj.getLastname());
            stmt.setString(3, obj.getDoa());
            stmt.setString(4, obj.getDob());
            stmt.setString(5, obj.getGender());
            stmt.setString(6, obj.getAddress());
            stmt.setString(7, obj.getPhone());
            stmt.setInt(8, obj.getSalary());
            stmt.setInt(9, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public int delete(Employee obj) {
        String sql = "DELETE FROM Employee WHERE employee_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;        
    }

    public List<Employee> getMSalary() {
        ArrayList<Employee> list = new ArrayList();
        String sql = "SELECT Employee.employee_id,Employee.firstname, Employee.lastname, daily_salary * count(TimeAttendance.ta_check_out_time) as salary FROM Employee\n" +
"INNER JOIN TimeAttendance\n" +
"ON Employee.employee_id = TimeAttendance.employee_id\n" +
"WHERE strftime('%m/%d/%Y', datetime(TimeAttendance.ta_check_out_time)) > strftime('%m/%d/%Y', 'now', 'start of month', '-1 month')\n" +
"GROUP BY Employee.employee_id";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                int id = rs.getInt("employee_id");
                String name = rs.getString("firstname");
                String lname = rs.getString("lastname");
                int salary = rs.getInt("salary");
                Employee emp = new Employee(id, name, lname, salary);
                list.add(emp);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
