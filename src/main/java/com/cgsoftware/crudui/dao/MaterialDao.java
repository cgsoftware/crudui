/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cgsoftware.crudui.dao;

import com.cgsoftware.crudui.helper.DatabaseHelper;
import com.cgsoftware.crudui.model.Material;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author informatics
 */
public class MaterialDao {
    public List<Material> getAll() {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM Material";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material mat = Material.fromRS(rs);
                list.add(mat);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Material> getMaterialByName(String name) {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM Material WHERE mat_name LIKE \'%" + name + "%\'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material mat = Material.fromRS(rs);
                list.add(mat);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public Material getMaterialById(int id) {
        Material mat = null;
        String sql = "SELECT * FROM Material WHERE mat_id=" + (id+1);
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            mat = Material.fromRS(rs);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return mat;
    }

    public Material update(Material material) {
        String sql = "UPDATE Material"
                + " SET mat_name = ?, mat_min = ?, mat_current = ?, mat_unit = ?, mat_price = ?"
                + " WHERE mat_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, material.getName());
            stmt.setInt(2, material.getMin());
            stmt.setInt(3, material.getCurrent());
            stmt.setString(4, material.getUnit());
            stmt.setInt(5, material.getMatPrice());
            stmt.setInt(6, material.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return material;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public Material save(Material material) {
        String sql = "INSERT INTO Material (mat_name, mat_min, mat_current, mat_unit, mat_price)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, material.getName());
            stmt.setInt(2, material.getMin());
            stmt.setInt(3, material.getCurrent());
            stmt.setString(4, material.getUnit());
            stmt.setInt(5, material.getMatPrice());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            material.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return material;
    }

    public int delete(Material obj) {
        String sql = "DELETE FROM Material WHERE mat_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1; 
    }
}
