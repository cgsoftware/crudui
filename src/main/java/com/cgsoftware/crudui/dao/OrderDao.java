/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgsoftware.crudui.dao;

import com.cgsoftware.crudui.helper.DatabaseHelper;
import com.cgsoftware.crudui.model.Order;
import com.cgsoftware.crudui.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Black Dragon
 */
public class OrderDao {

    public Order save(Order obj) {

        String sql = "INSERT INTO Orders (product_name, total_price, date, total_discount, employee_id, customer_id)"
                + "VALUES(?, ?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getProdName());
            stmt.setInt(2, obj.getPrice());
            stmt.setString(3, obj.getDate());
            stmt.setInt(4, obj.getDiscount());
            stmt.setString(5, obj.getEmpId());
            stmt.setInt(6, obj.getCusId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    public List<Order> getAll() {
        ArrayList<Order> list = new ArrayList();
        String sql = "SELECT * FROM Orders";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Order order = Order.fromRS(rs);
                list.add(order);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public void delete(Order obj) {
        String sql = "DELETE FROM Orders WHERE order_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public void update(Order obj) {
        String sql = "UPDATE Orders"
                + " SET product_name = ?, total_price = ?, date = ?, total_discount = ?, employee_id = ?, customer_id = ?"
                + " WHERE order_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getProdName());
            stmt.setInt(2, obj.getPrice());
            stmt.setString(3, obj.getDate());
            stmt.setInt(4, obj.getDiscount());
            stmt.setString(5, obj.getEmpId());
            stmt.setInt(6, obj.getCusId());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public String getSale() {
        String query = "";
        String sql = "SELECT SUM(total_price) as sales from Orders\n"
                + "WHERE total_price > 0";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            query = rs.getString("sales");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return query;
    }

    public String getLoss() {
        String query = "";
        String sql = "SELECT SUM(total_price) as loss from Orders\n"
                + "WHERE total_price < 0";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            query = rs.getString("loss");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return query;
    }

    public String getProfit() {
        String query = "";
        String sql = "SELECT SUM(total_price) as profit from Orders";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            query = rs.getString("profit");

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return query;
    }

    public ArrayList<Product> getTop() {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT product_name, SUM(total_price) as top_sales from Orders\n"
                + "WHERE total_price > 0\n"
                + "GROUP BY product_name\n"
                + "ORDER BY top_sales DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            int c = 0;
            while (rs.next()) {
                if (c == 5) {
                    break;
                }
                Product prod = new Product(rs.getString("product_name"), rs.getInt("top_sales"));
                list.add(prod);
                c++;
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public ArrayList<Product> getLeast() {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT product_name, SUM(total_price) as least_sales from Orders\n"
                + "WHERE total_price > 0\n"
                + "GROUP BY product_name\n"
                + "ORDER BY least_sales ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            int c = 0;
            while (rs.next()) {
                if (c == 5) {
                    break;
                }
                Product prod = new Product(rs.getString("product_name"), rs.getInt("least_sales"));
                list.add(prod);
                c++;
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
