/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgsoftware.crudui;

import com.cgsoftware.crudui.model.Order;
import com.cgsoftware.crudui.service.OrderService;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ACER
 */
public class OrderManagement extends javax.swing.JPanel {

    private final OrderService orderService;
    private List<Order> list;
    private Order editedOrder;

    /**
     * Creates new form OrderManagement
     */
    public OrderManagement() {
        initComponents();
        orderService = new OrderService();

        list = orderService.getOrder();
        orderTable.setModel(new AbstractTableModel() {
            String[] columnNames = {"ID", "Product name", "Total Price", "Date", "Discount", "Employee ID", "Customer ID"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return list.size();
            }

            @Override
            public int getColumnCount() {
                return columnNames.length;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Order order = list.get(rowIndex);
                switch (columnIndex) {
                    case 0:
                        return order.getId();
                    case 1:
                        return order.getProdName();
                    case 2:
                        return order.getPrice();
                    case 3:
                        return order.getDate();
                    case 4:
                        return order.getDiscount();
                    case 5:
                        return order.getEmpId();
                    case 6:
                        return order.getCusId();
                    default:
                        return "Unknown";
                }
            }
        });

        enableForm(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtDate = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txtCustomer = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtPrice = new javax.swing.JTextField();
        txtEmp = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtDiscount = new javax.swing.JTextField();
        btnSave = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        orderTable = new javax.swing.JTable();
        btnAdd = new javax.swing.JButton();
        btnEdit = new javax.swing.JButton();
        btnDelete = new javax.swing.JButton();

        setBackground(new java.awt.Color(220, 189, 158));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel1.setText("product_name :");
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 10, 100, 20));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel2.setText("total_discount :");
        add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, 110, 20));
        add(txtDate, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 10, 320, -1));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel3.setText("customer_id :");
        add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(890, 10, 90, 20));
        add(txtCustomer, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 10, 320, -1));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel4.setText("date :");
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 10, 76, -1));
        add(txtName, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 10, 270, -1));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel5.setText("total_price :");
        jLabel5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 40, 80, 20));
        add(txtPrice, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 40, 320, -1));

        txtEmp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmpActionPerformed(evt);
            }
        });
        add(txtEmp, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 40, 320, -1));

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel7.setText("employee_id :");
        jLabel7.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 40, 80, 20));
        add(txtDiscount, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 40, 270, -1));

        btnSave.setText("Save");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        add(btnSave, new org.netbeans.lib.awtextra.AbsoluteConstraints(1200, 100, -1, -1));

        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });
        add(btnClear, new org.netbeans.lib.awtextra.AbsoluteConstraints(1280, 100, -1, -1));

        orderTable.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        orderTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(orderTable);

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 163, 1620, 580));

        btnAdd.setText("Add");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });
        add(btnAdd, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 790, -1, -1));

        btnEdit.setText("Edit");
        btnEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditActionPerformed(evt);
            }
        });
        add(btnEdit, new org.netbeans.lib.awtextra.AbsoluteConstraints(210, 790, -1, -1));

        btnDelete.setText("Delete");
        btnDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeleteActionPerformed(evt);
            }
        });
        add(btnDelete, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 790, -1, -1));
    }// </editor-fold>//GEN-END:initComponents

    private void txtEmpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmpActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmpActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        txtName.setText("");
        txtPrice.setText("");
        txtDate.setText("");
        txtDiscount.setText("");
        txtEmp.setText("");
        txtCustomer.setText("");
    }//GEN-LAST:event_btnClearActionPerformed

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if (editedOrder.getId() < 0) {
            editedOrder.setProdName(txtName.getText());
            editedOrder.setPrice(Integer.parseInt(txtPrice.getText()));
            editedOrder.setDate(txtDate.getText());
            editedOrder.setDiscount(Integer.parseInt(txtDiscount.getText()));
            editedOrder.setEmpId(txtEmp.getText());
            editedOrder.setCusId(Integer.parseInt(txtCustomer.getText()));
            enableForm(false);
            orderService.addNew(editedOrder);
            refreshTable();
        } else {
            editedOrder.setProdName(txtName.getText());
            editedOrder.setPrice(Integer.parseInt(txtPrice.getText()));
            editedOrder.setDate(txtDate.getText());
            editedOrder.setDiscount(Integer.parseInt(txtDiscount.getText()));
            editedOrder.setEmpId(txtEmp.getText());
            editedOrder.setCusId(Integer.parseInt(txtCustomer.getText()));
            enableForm(false);
            orderService.update(editedOrder);
            refreshTable();
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        editedOrder = new Order();
        enableForm(true);
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditActionPerformed
        int selectedIndex = orderTable.getSelectedRow();
        if (selectedIndex >= 0) {
            editedOrder = list.get(selectedIndex);
            txtName.setText("" + editedOrder.getProdName());
            txtPrice.setText("" + editedOrder.getPrice());
            txtDate.setText("" + editedOrder.getDate());
            txtDiscount.setText("" + editedOrder.getDiscount());
            txtEmp.setText("" + editedOrder.getEmpId());
            txtCustomer.setText("" + editedOrder.getCusId());
            enableForm(true);
        }
    }//GEN-LAST:event_btnEditActionPerformed

    private void btnDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeleteActionPerformed
        int selectedIndex = orderTable.getSelectedRow();
        if (selectedIndex >= 0) {
            editedOrder = list.get(selectedIndex);
            int input = JOptionPane.showConfirmDialog(this, "Do you want to proceed?", "Select an Option...",
                    JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
            if (input == 0) {
                orderService.delete(editedOrder);
            }
            refreshTable();
        }
    }//GEN-LAST:event_btnDeleteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnClear;
    private javax.swing.JButton btnDelete;
    private javax.swing.JButton btnEdit;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable orderTable;
    private javax.swing.JTextField txtCustomer;
    private javax.swing.JTextField txtDate;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JTextField txtEmp;
    private javax.swing.JTextField txtName;
    private javax.swing.JTextField txtPrice;
    // End of variables declaration//GEN-END:variables

    private void enableForm(boolean status) {
        if (status == false) {
            txtName.setText("");
            txtPrice.setText("");
            txtDate.setText("");
            txtDiscount.setText("");
            txtEmp.setText("");
            txtCustomer.setText("");
        }
        txtName.setEnabled(status);
        txtPrice.setEnabled(status);
        txtDate.setEnabled(status);
        txtDiscount.setEnabled(status);
        txtEmp.setEnabled(status);
        txtCustomer.setEnabled(status);
        btnSave.setEnabled(status);
        btnClear.setEnabled(status);
        txtName.requestFocus();

    }

    private void refreshTable() {
        list = orderService.getOrder();
        orderTable.revalidate();
        orderTable.repaint();
    }
}
