/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cgsoftware.crudui.service;

import com.cgsoftware.crudui.dao.EmployeeDao;
import com.cgsoftware.crudui.model.Employee;
import java.util.List;

/**
 *
 * @author informatics
 */
public class EmployeeService {
    public List<Employee> getEmployee(){
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getAll();
    }
    
    public Employee addNew(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.save(editedEmployee);
    }
    
    
    public Employee update(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.delete(editedEmployee);
    }

    public List<Employee> getEmployeeMSalary() {
        EmployeeDao employeeDao = new EmployeeDao();
        return employeeDao.getMSalary();
    }
}
