/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgsoftware.crudui.service;

import com.cgsoftware.crudui.dao.ProductDao;
import com.cgsoftware.crudui.model.Product;
import java.util.List;

/**
 *
 * @author Black Dragon
 */
public class ProductService {
    public List<Product> getAllProduct(){
        ProductDao productDao = new ProductDao();
        return productDao.getAll();
    }

    public void delete(Product editedProduct) {
        ProductDao productDao = new ProductDao();
        productDao.delete(editedProduct);
    }

    public void addNew(Product editedProduct) {
        ProductDao prodDao = new ProductDao();
        prodDao.save(editedProduct);
    }

    public void update(Product editedProduct) {
        ProductDao prodDao = new ProductDao();
        prodDao.update(editedProduct);
    }
}
