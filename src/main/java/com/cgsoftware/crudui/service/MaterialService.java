/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cgsoftware.crudui.service;

import com.cgsoftware.crudui.dao.MaterialDao;
import com.cgsoftware.crudui.model.Material;
import java.util.List;

/**
 *
 * @author informatics
 */
public class MaterialService {
    public List<Material> getAllMaterial(){
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getAll();
    }
    
    public List<Material> getMaterialByName(String name) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getMaterialByName(name);
    }

    public Material getMaterialById(int id) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.getMaterialById(id);
    }
    
    public Material update(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.update(editedMaterial);
    }
    
    public Material addNew(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.save(editedMaterial);
    }

    public int delete(Material editedMaterial) {
        MaterialDao materialDao = new MaterialDao();
        return materialDao.delete(editedMaterial);
    }
}
