/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cgsoftware.crudui.service;

import com.cgsoftware.crudui.dao.CustomerDao;
import com.cgsoftware.crudui.model.Customer;
import java.util.List;

/**
 *
 * @author informatics
 */
public class CustomerService {
    public List<Customer> getAllCustomers(){
        CustomerDao customerDao = new CustomerDao();
        return customerDao.getAll();
    }
    
    public Customer getCustomer(String phone){
        CustomerDao customerDao = new CustomerDao();
        return customerDao.getByPhone(phone);
    }

    public Customer addNew(Customer editedCus) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.save(editedCus);
    }

    public Customer update(Customer editedCus) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.update(editedCus);
    }

    public int delete(Customer editedCus) {
        CustomerDao customerDao = new CustomerDao();
        return customerDao.delete(editedCus);
    }
}
