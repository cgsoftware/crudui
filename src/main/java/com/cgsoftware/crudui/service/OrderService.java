/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgsoftware.crudui.service;

import com.cgsoftware.crudui.dao.OrderDao;
import com.cgsoftware.crudui.model.Order;
import com.cgsoftware.crudui.model.Product;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Black Dragon
 */
public class OrderService {
    public Order addNew(Order order) {
        OrderDao orderDao = new OrderDao();
        return orderDao.save(order);
    }

    public List<Order> getOrder() {
        OrderDao orderDao = new OrderDao();
        return orderDao.getAll();
    }

    public void delete(Order editedOrder) {
        OrderDao orderDao = new OrderDao();
        orderDao.delete(editedOrder);
    }

    public void update(Order editedOrder) {
        OrderDao orderDao = new OrderDao();
        orderDao.update(editedOrder);
    }

    public String getSale() {
        OrderDao orderDao = new OrderDao();
        return orderDao.getSale();
    }

    public String getLoss() {
        OrderDao orderDao = new OrderDao();
        return orderDao.getLoss();
    }

    public String getProfit() {
        OrderDao orderDao = new OrderDao();
        return orderDao.getProfit();
    }

    public ArrayList<Product> getTop() {
        OrderDao orderDao = new OrderDao();
        return orderDao.getTop();
    }

    public ArrayList<Product> getLeast() {
        OrderDao orderDao = new OrderDao();
        return orderDao.getLeast();
    }
}
