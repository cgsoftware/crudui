/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cgsoftware.crudui.service;

import com.cgsoftware.crudui.dao.TimeAttendanceDao;
import com.cgsoftware.crudui.model.TimeAttendance;

/**
 *
 * @author Black Dragon
 */
public class TimeAttendanceService {
    public TimeAttendance addNew(TimeAttendance ta) {
        TimeAttendanceDao taDao = new TimeAttendanceDao();
        return taDao.save(ta);
    }

    public void checkOut(int userId, TimeAttendance ta) {
        TimeAttendanceDao taDao = new TimeAttendanceDao();
        taDao.checkOut(userId, ta);
    }

    public TimeAttendance getTa(int userId) {
        TimeAttendanceDao taDao = new TimeAttendanceDao();
        return taDao.getWhere(userId);
    }
}
