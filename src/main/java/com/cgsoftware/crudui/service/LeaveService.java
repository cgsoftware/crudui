/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.cgsoftware.crudui.service;

import com.cgsoftware.crudui.dao.LeaveDao;
import com.cgsoftware.crudui.model.Leave;
import java.util.List;

/**
 *
 * @author informatics
 */
public class LeaveService {
    public List<Leave> getAll(){
        LeaveDao leaveDao = new LeaveDao();
        return leaveDao.getAll();
    }

    public Leave addNew(Leave editedLeave) {
        LeaveDao leaveDao = new LeaveDao();
        return leaveDao.save(editedLeave);
    }

    public Leave update(Leave editedLeave) {
        LeaveDao leaveDao = new LeaveDao();
        return leaveDao.update(editedLeave);
    }

    public int delete(Leave editedLeave) {
        LeaveDao leaveDao = new LeaveDao();
        return leaveDao.delete(editedLeave);
    }
}
